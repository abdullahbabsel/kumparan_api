<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
	/* News */
	$router->get('news',  ['uses' => 'NewsController@showAllNews']);
	$router->get('news/{id}', ['uses' => 'NewsController@showOneNews']);
	$router->post('news', ['uses' => 'NewsController@create']);
	$router->delete('news/{id}', ['uses' => 'NewsController@delete']);
	$router->put('news/{id}', ['uses' => 'NewsController@update']);
	/* Topic */
	$router->get('topic',  ['uses' => 'TopicController@showAllTopic']);
	$router->get('topic/{id}', ['uses' => 'TopicController@showOneTopic']);
	$router->post('topic', ['uses' => 'TopicController@create']);
	$router->delete('topic/{id}', ['uses' => 'TopicController@delete']);
	$router->put('topic/{id}', ['uses' => 'TopicController@update']);
	/* NewsTopic */
	$router->post('news_topic', ['uses' => 'NewsTopicController@create']);
	$router->delete('news_topic/{id}', ['uses' => 'NewsTopicController@delete']);
});