<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTopic extends Model
{	
	protected $table = 'tbl_news_topic';
	
	public function topic()
    {
        return $this->belongsTo('App\Topic');
    }
	
    protected $fillable = [
        'news_id', 'topic_id'
    ];
}