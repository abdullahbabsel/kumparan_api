<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
	use SoftDeletes;
	
	protected $table = 'tbl_topic';
	
    protected $fillable = [
        'title', 'slug'
    ];

    protected $hidden = [];
	
	protected $dates = ['deleted_at'];
}