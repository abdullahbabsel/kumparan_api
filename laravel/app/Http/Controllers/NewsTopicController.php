<?php

namespace App\Http\Controllers;

use App\NewsTopic;
use Illuminate\Http\Request;

class NewsTopicController extends Controller
{
    public function create(Request $request)
    {
        /* $newsTopic = NewsTopic::create($request->all()); */
		$newsTopic = NewsTopic::insert($request->all());

        return response()->json($newsTopic, 201);
    }

    public function delete($id)
    {
        /* NewsTopic::findOrFail($id)->delete(); */
        NewsTopic::where('news_id', $id)->delete();
        return response('Deleted Successfully', 200);
    }
}