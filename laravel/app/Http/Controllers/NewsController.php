<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function showAllNews(Request $request)
    {		
		$conditions = [];
		
        if($request->input('title'))
			$conditions[] = ['title', 'like', '%'.$request->input('title').'%'];
		if($request->input('publish'))
            $conditions[] = ['publish', '=', $request->input('publish')];
        if($request->input('topic_id'))
			$conditions[] = ['topic_id_tmp', 'like', '%"'.$request->input('topic_id').'"%'];
		
		if($request->input()) {
			return response()->json(News::orderBy('id', 'DESC')->with(['newsTopic.topic'])->where($conditions)->get());
		} else {
			return response()->json(News::orderBy('id', 'DESC')->with(['newsTopic.topic'])->get());
		}
    }

    public function showOneNews($id)
    {
        return response()->json(News::find($id));
    }

    public function create(Request $request)
    {
        $news = News::create($request->all());

        return response()->json($news, 201);
    }

    public function update($id, Request $request)
    {
        $news = News::findOrFail($id);
        $news->update($request->all());

        return response()->json($news, 200);
    }

    public function delete($id)
    {
        News::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}