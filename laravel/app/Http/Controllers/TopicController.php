<?php

namespace App\Http\Controllers;

use App\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{

    public function showAllTopic()
    {
        return response()->json(Topic::orderBy('id', 'DESC')->get());
    }

    public function showOneTopic($id)
    {
        return response()->json(Topic::find($id));
    }

    public function create(Request $request)
    {
        $topic = Topic::create($request->all());

        return response()->json($topic, 201);
    }

    public function update($id, Request $request)
    {
        $topic = Topic::findOrFail($id);
        $topic->update($request->all());

        return response()->json($topic, 200);
    }

    public function delete($id)
    {
        Topic::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}